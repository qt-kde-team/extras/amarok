Source: amarok
Section: sound
Priority: optional
Maintainer: Debian KDE Extras Team <pkg-kde-extras@lists.alioth.debian.org>
Uploaders: Pino Toscano <pino@debian.org>,
Build-Depends: debhelper-compat (= 13),
 dh-exec,
 dh-sequence-kf5,
 cmake (>= 3.16~),
 gettext,
 pkgconf,
 python3:any,
 libtag-dev (>= 1.12~),
 qtbase5-dev (>= 5.15.0~),
 qttools5-dev (>= 5.15.0~),
 qtdeclarative5-dev (>= 5.15.0~),
 libqt5svg5-dev (>= 5.15.0~),
 qtquickcontrols2-5-dev (>= 5.15.0~),
 qtwebengine5-dev (>= 5.15.0~) [amd64 arm64 armhf i386],
 libphonon4qt5-dev,
 libphonon4qt5experimental-dev,
 extra-cmake-modules (>= 5.108.0~),
 libkf5archive-dev (>= 5.108.0~),
 libkf5codecs-dev (>= 5.108.0~),
 libkf5config-dev (>= 5.108.0~),
 libkf5configwidgets-dev (>= 5.108.0~),
 libkf5coreaddons-dev (>= 5.108.0~),
 libkf5crash-dev (>= 5.108.0~),
 libkf5dbusaddons-dev (>= 5.108.0~),
 libkf5dnssd-dev (>= 5.108.0~),
 libkf5doctools-dev (>= 5.108.0~),
 libkf5globalaccel-dev (>= 5.108.0~),
 libkf5guiaddons-dev (>= 5.108.0~),
 libkf5i18n-dev (>= 5.108.0~),
 libkf5iconthemes-dev (>= 5.108.0~),
 libkf5kcmutils-dev (>= 5.108.0~),
 libkf5kio-dev (>= 5.108.0~),
 libkf5notifications-dev (>= 5.108.0~),
 libkf5package-dev (>= 5.108.0~),
 libkf5solid-dev (>= 5.108.0~),
 libkf5texteditor-dev (>= 5.108.0~),
 libkf5textwidgets-dev (>= 5.108.0~),
 libkf5threadweaver-dev (>= 5.108.0~),
 libkf5wallet-dev (>= 5.108.0~),
 libkf5widgetsaddons-dev (>= 5.108.0~),
 libkf5windowsystem-dev (>= 5.108.0~),
 kirigami2-dev,
 libmtp-dev (>= 1.0.0),
 libglib2.0-dev, libgpod-dev (>= 0.7.0),
 libmariadbd-dev, libmariadb-dev,
 libqca-qt5-2-dev, liblastfm5-dev (>= 1.0.3),
 libavformat-dev (>= 4:0.5), libofa0-dev,
 libmygpo-qt-dev,
 libgdk-pixbuf-2.0-dev,
Build-Depends-Indep: mariadb-server-core
Standards-Version: 4.7.0
Rules-Requires-Root: no
Homepage: https://amarok.kde.org
Vcs-Git: https://salsa.debian.org/qt-kde-team/extras/amarok.git
Vcs-Browser: https://salsa.debian.org/qt-kde-team/extras/amarok

Package: amarok
Architecture: any
Depends: amarok-common (= ${source:Version}), amarok-utils (= ${binary:Version}),
 qml-module-org-kde-kirigami2 (>= 5.15.0~),
 qml-module-qtqml-models2 (>= 5.15.0~),
 qml-module-qtquick-controls2 (>= 5.15.0~),
 qml-module-qtquick-dialogs (>= 5.15.0~),
 qml-module-qtquick-layouts (>= 5.15.0~),
 qml-module-qtquick2 (>= 5.15.0~),
 qml-module-qtwebengine (>= 5.15.0~) [amd64 arm64 armhf i386],
 ${shlibs:Depends}, ${misc:Depends},
Recommends: kio-audiocd,
Suggests: amarok-doc (>= ${source:Version}), libqt5sql5-sqlite, libqt5sql5-mysql, libqt5sql5-psql,
 moodbar
Description: easy to use media player based on the KDE Platform
 Amarok is a powerful music player with an intuitive interface. It makes
 playing the music you love and discovering new music easier than ever before
 and it looks good doing it! Amarok is based on the powerful Qt4 / KDE4
 Platform and nicely integrates with KDE desktop.
 .
 Much work has been invested into integrating Amarok 2 with various Web
 services:
   - Ampache
   - Jamendo Service
   - Last.fm
   - Librivox
   - MP3tunes
   - Magnatune
   - OPML Podcast Directory
 .
 Amarok comes with a lot of features including but not limited to:
   - Scripts - enhance your Amarok experience with community developed scripts.
   - Dynamic Playlists - create playlists that automatically update.
   - Context View - customize interface with the Plasma powered Context View.
   - PopUp Dropper - simplify drag&drop actions with revolutionary menu system.
   - Multiple Language Translations
   - Collection Management - organizing your music collection has never been
     easier with Amarok's powerful tagging, renaming, and sorting abilities.
   - Database Importing - import collections from Amarok 1.4 or iTunes.
   - Scriptable Services - integrate other web services into Amarok.

Package: amarok-common
Architecture: all
Depends: ${misc:Depends},
Recommends: amarok (>= ${source:Version})
Description: architecture independent files for Amarok
 This package contains architecture independent files needed for Amarok to run
 properly. Therefore, unless you have 'amarok' package of the same version
 installed, you will hardly find this package useful.
 .
 Amarok is a powerful music player with an intuitive interface.

Package: amarok-doc
Section: doc
Architecture: all
Depends: ${misc:Depends}
Recommends: amarok (>= ${source:Version}), khelpcenter
Description: Amarok documentation (Handbook)
 This package contains Amarok user documentation in various languages. It can be
 opened from the application menu Help -> Amarok Handbook.
 .
 Amarok is a powerful music player with an intuitive interface.

Package: amarok-utils
Section: utils
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
Description: utilities for Amarok media player
 This package contains command line utilities that are typically used by Amarok
 media player but might also be useful on systems without Amarok installed.
 They are designed to be lightweight as they do not depend on KDE libraries.
 .
 Currently the package contains the following utilities:
   - amarokcollectionscanner - scans audio files, collects information from
     file tags and prints it in the structured XML format.
   - amarok_afttagger - a helper program which writes/removes custom tags
     to/from media files required for embedded "Amarok File Tracking".
